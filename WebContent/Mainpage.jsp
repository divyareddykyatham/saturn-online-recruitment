<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<style>
.jumbotron {
    position: relative;
    background-size: cover;
    height : 150px;
    overflow: hidden;
    background-color:#126180;
}
.jumbotronwidth{
top :0px;
max-width : 30%;
}
.navbar{
background-color: #126180;
}

</style>
</head>

<body class = "body">
<div class="container">
  <div class="jumbotron">
   <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 pull-right "> <img vspace="20" class="jumbotronwidth" alt=" " src="./images/talentsprintimg.jpg"> </div>
    <h2 align = center style = "color : white"><b><i>Online Recruitment System</i></b></h2>                        
  </div>
  </div>
 
  <nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#"><font color = "white"><b>Home</b></a>
    </div>
    <div>
      <ul class="nav navbar-nav">

        <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#"><font color = white><b>News</b></font> <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="#">Page 1</a></li>
            <li><a href="#">Page 1</a></li>
            <li><a href="#">Page 1</a></li>
          </ul>
        </li>
        <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#"><font color = white><b>Syllabus</b></font> <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="#">Page 1</a></li>
            <li><a href="#">Page 1</a></li>
            <li><a href="#">Page 1</a></li>
          </ul>
        </li>
        
          <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#"><font color = white><b>Online Test Papers</b></font><span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="#">Page 1</a></li>
            <li><a href="#">Page 1</a></li>
            <li><a href="#">Page 1</a></li>
          </ul>
        </li>
        <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#"><font color = white><b>Updates</b></font><span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="#">Page 1</a></li>
            <li><a href="#">Page 1</a></li>
            <li><a href="#">Page 1</a></li>
          </ul>
        </li>
      </ul>
      
      <ul class="nav navbar-nav navbar-right">
        <li class="dropdown"><a  href="signup.jsp"><span class="glyphicon glyphicon-user"></span> <font color = white><b>SignUp</b></font></a>
        </li>
        <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#"><span class="glyphicon glyphicon-log-in"></span> <font color = white><b>Login</b></font></a>
        <ul class="dropdown-menu">
            <li><a href="elogin.jsp">Employer Login</a></li>
            <li><a href="ulogin.jsp">User Login</a></li>
          </ul>
        </li>
      </ul>
      
    </div>
  </div>
</nav>
  

</body>
</html>